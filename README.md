| 项目    | 端口   | 描述    |
| ------ | ------ | ------ |
| spring-cloud-eureka-server | 8081 | 服务注册中心 |
| spring-cloud-eureka-client | 8082 | 服务调用 |
| spring-cloud-gateway | 8083 | 网关 |
| spring-cloud-feign | 8084 | feign-调用方|
| spring-cloud-ribbon | 8085 | ribbon-调用方 |
| spring-cloud-config | 8086 | 分布式文件配置中心 |
| spring-boot-admin | 8087 | spring boot admin |


![design](https://images.gitee.com/uploads/images/2018/1107/110849_82ad6888_1045387.png "design.png")
![spring boot admin](https://images.gitee.com/uploads/images/2019/0326/105224_a9cd2c4a_1045387.png "WX20190326-105124@2x.png")
![spring boot admin](https://images.gitee.com/uploads/images/2019/0326/105237_bbb3d62a_1045387.png "WX20190326-105135@2x.png")